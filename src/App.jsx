import React from 'react';
import questions from './data';
import Question from './components/Question';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      score: 0,
    };
    this.handleAnswer = this.handleAnswer.bind(this);
  }

  handleAnswer(selectedAnswer) {
    if (selectedAnswer.isCorrect) {
      // TODO: increment score 
    }
  }

  render() {
    return (
      <div className="container">
        <h1 className="app-title">💩 Shitty Quiz App</h1>
        <div className="questions">
          {questions.map((question) => (
            <Question
              key={question.id}
              onAnswer={this.handleAnswer}
              {...question}
            />
          ))}
        </div>
        <div className="action">
          <button className="submit-button">Submit</button>
        </div>
      </div>
    );
  }
}

export default App;
